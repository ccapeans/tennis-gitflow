
public class TennisGame1 implements TennisGame {
    private String player1Name;
    private int player1Score;

    private String player2Name;
    private int player2Score;

    private String[] scoreNaming;

    public TennisGame1(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player1Score = 0;

        this.player2Name = player2Name;
        this.player2Score = 0;

        this.scoreNaming = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    }

    public void addPointTo(String playerName) {
        if (playerName == "player1")
            player1Score += 1;
        else
            player2Score += 1;
    }

    public String getScore() {
        if (isScoreTied()) {
            if (isDeuce()) {
                return "Deuce";
            }
            return this.scoreNaming[player1Score] + "-All";
        }
        return this.getWhoIsWinning();
    }

    private boolean isScoreTied() {
        return this.player1Score == this.player2Score;
    }

    private boolean isDeuce() {
        return this.player1Score == this.player2Score && this.player1Score >= 3;
    }

    private String getWhoIsWinning() {
        if (hasAnyPlayerMoreThan3Points()) {
            if (this.getScoreDifference() == 1) {
                if (isWinning(player1Name)) return "Advantage player1";
                return "Advantage player2";
            }

            if (isWinning(player1Name)) return "Win for player1";
            return "Win for player2";

        }
        return this.toString();
    }

    private boolean hasAnyPlayerMoreThan3Points() {
        return this.player1Score >= 4 || this.player2Score >= 4;
    }

    private boolean isWinning(String playerName) {
        if (playerName == this.player1Name && player1Score > player2Score) return true;
        else if (playerName == this.player2Name && player2Score > player1Score) return true;
        return false;
    }

    private int getScoreDifference() {
        return Math.abs(this.player1Score - this.player2Score);
    }

    public String toString() {
        return this.scoreNaming[this.player1Score] + "-" + this.scoreNaming[this.player2Score];
    }
}
