
public interface TennisGame {
    void addPointTo(String playerName);
    String getScore();
}